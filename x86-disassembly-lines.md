- eax is the temporary register used in x86, much like arm's $t registers
- two functions end in jump instructions because it is the last instruction in that function
- registers are not numbered in x86, but they have specific names
- at the end of the main function, x86 will reset eax to all 0s for the next program

line 1: Declares the function sum2<br>
line 2: Using lea, we can add the values in rdi and rsi while storing it in eax instead of rdi or rsi<br>
line 3: Ends the function

line 4: Declares the function print_the_value<br>
line 5: moves edi, which contains the result from sum2, to esi because edi will need to be used for a string<br>
line 6: sets edi to .L.str, which contains "%d" as a way to show that esi contains the value we want<br>
line 7: xor eax to itself changes all bits to 0, clearing eax for other programs<br>
line 8: calls printf, done as a jmp instruction because this is the last instruction of print_the_value

line 9: Declares the function entry_point<br>
line 10: Sets rbx to this address as the start of the frame<br>
line 11: calls rand to find a random integer for variable a<br>
line 12: moves eax, which contains the result from rand, into ebx to hold the value<br>
line 13: calls rand to find a random integer for variable b<br>
line 14: moves eax, which contains the result from rand, into edi for sum2 when it is called later<br>
line 15: moves ebx, which contains the variable a, into esi for sum2 when it is called later.<br>
line 16: calls sum2 to calculate the sum of variable a and b<br>
line 17: moves the result of sum2 to edi for when print_the_value is called later<br>
line 18: pops rbx to free up the frame since we are done with arithmetics<br>
line 19: calls print_the_value, done as a jmp instruction because this is the last instruction of entry_point<br>

![code.png](code.png)
